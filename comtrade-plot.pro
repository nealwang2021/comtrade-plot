TEMPLATE = app
TARGET = comtrade-plot

CONFIG += c++11

QT += widgets printsupport network
#有些编译器或者Qt版本不支持下面这行,需要注释掉
PRECOMPILED_HEADER = ./src/head.h
#CONFIG      += warn_off
#HEADERS     += head.h
INCLUDEPATH += $$PWD/vendor
INCLUDEPATH += $$PWD/src

#include ($$PWD/plotdemo/plotdemo.pri)
#INCLUDEPATH += $$PWD/plotdemo/
# Input
SOURCES += src/main.cpp \
           src/ComtradeAnalogChannel.cpp \
           src/ComtradeChannel.cpp \
           src/ComtradeDigitalChannel.cpp \
           src/ComtradeRecord.cpp \
           src/PlotWindow.cpp \
           src/quiwidget.cpp \
           src/singlechannel.cpp \
           vendor/qcustomplot.cpp

HEADERS += src/ComtradeAnalogChannel.h \
           src/ComtradeChannel.h \
           src/ComtradeDigitalChannel.h \
           src/ComtradeRecord.h \
           src/PlotWindow.h \
           src/quiwidget.h \
           src/singlechannel.h\
           vendor/qcustomplot.h

FORMS += \
    src/singlechannel.ui\
    ui/plotwindow.ui

isEmpty(PREFIX) {
    PREFIX = /usr/local
}

unix:target.path = $$PREFIX/bin
unix:INSTALLS += target

RESOURCES += \
    resource.qrc
