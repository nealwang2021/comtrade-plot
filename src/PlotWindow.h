﻿#ifndef _PLOT_WINDOW_H
#define _PLOT_WINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QFile>
#include "quiwidget.h"
#include "ComtradeAnalogChannel.h"
#include "ComtradeDigitalChannel.h"
#include "ComtradeRecord.h"
#include "qcustomplot.h"
#include "singlechannel.h"


namespace Ui {
class PlotWindow;
}

class PlotWindow : public QMainWindow
{
    Q_OBJECT

public:


    explicit PlotWindow(QWidget *parent = nullptr);
    ~PlotWindow();

    void setComtradeRecord(ComtradeRecord *comtradeRecord);
    bool loadSamples(QIODevice *samplesFile);

    void initComtrade(QString cfgPath,QString datPath);



private slots:
    void on_channel2_windowIconChanged(const QIcon &icon);

    void on_btn_openFile_released();

private:
    Ui::PlotWindow *ui;

    ComtradeRecord *m_comtradeRecord;
    QList<QVector<double>> m_analogSamples;
    QList<QVector<double>> m_digitalSamples;
    QVector<double> m_timeBaseus;
    QList<SingleChannel*> m_plotlist;
};

#endif
