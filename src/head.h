﻿#ifdef ASIO_STANDALONE
#define WIN32_LEAN_AND_MEAN
#include"asio.hpp"
#endif
#include <QtCore>
#include <QtGui>
//#include <QtSql>
#include <QtNetwork>
//#include <QtScript>

#if (QT_VERSION > QT_VERSION_CHECK(5,0,0))
#include <QtWidgets>
//#include <QtPrintSupport>
#endif

#ifdef webkit
#include <QtWebKit>
#if (QT_VERSION > QT_VERSION_CHECK(5,0,0))
#include <QtWebKitWidgets>
#endif
#elif webengine
#include <QtWebEngineWidgets>
#elif webie
#include <QAxWidget>
#endif

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif


