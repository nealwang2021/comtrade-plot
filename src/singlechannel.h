﻿#ifndef SINGLECHANNEL_H
#define SINGLECHANNEL_H

#include <QWidget>
#include <QMouseEvent>
#include <QEvent>
#include <QRubberBand>
#include "quiwidget.h"
#include "qcustomplot.h"
#include "ComtradeAnalogChannel.h"
#include "ComtradeDigitalChannel.h"
#include "ComtradeRecord.h"

class ChWaveInfo{
public:
    int ChNumber;       // 通道号
    float Frequency;    // 采集频率
    float SampTime;     // 采集时长，单位：s
    double Value;        // 值
    ChWaveInfo(int ChNum = 0, float Freq = 0, float Time = 0, double Val = 0) :
        ChNumber(ChNum),
        Frequency(Freq),
        SampTime(Time),
        Value(Val) {}
};

namespace Ui {
class SingleChannel;
}

class SingleChannel : public QWidget
{
    Q_OBJECT

public:
    enum ChannelType {
        AnalogChannel = 0,
        DigitalChannel = 1
    };
    Q_ENUM(ChannelType)
    explicit SingleChannel(QWidget *parent = nullptr);
    ~SingleChannel();
    void addPlot(ChannelType type, int index, bool primary, ComtradeRecord * m_comtradeRecord,QList<QVector<double>> m_analogSamples,QList<QVector<double>> m_digitalSamples,QVector<double> m_timeBaseus);
    void setRangeLower(int index,int sample,QVector<double> m_timeBaseus);
    void setRangeUpper(int index,int sample,QVector<double> m_timeBaseus);

    bool savePng(QCustomPlot *plot,const QString &fileName, int width = 0, int height = 0);
    void setupAdvancedAxesDemo(QCustomPlot *customPlot);
    void clearPlot();

private slots:
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void contextMenuRequest(QPoint pos);
    //调整到合适范围
    void rescaleAxes();

    void slot_selectionChangedByUser();

    void on_btn_zoomIn_clicked(bool checked);

    void on_btn_select_clicked(bool checked);

    void on_btn_move_clicked(bool checked);
private slots:
  void horzScrollBarChanged(int value);
  void xAxisChanged(QCPRange range);


protected:
    void Init();

    QString GetWaveInfo(const ChWaveInfo& Info);
private:
    Ui::SingleChannel *ui;
protected:
    QRubberBand *RubBand;
    QPoint StartPoint;
    ChWaveInfo WaveInfo;
    double Xup;
    double Xdown;
    double Yup;
    double Ydown;
    QStringList m_selectdata;
    QList<QColor> m_colorlist;
    QList<QPointF> pointlist[5];
};

#endif // SINGLECHANNEL_H
