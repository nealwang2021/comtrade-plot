﻿#include "singlechannel.h"
#include "ui_singlechannel.h"
#include<iostream>
using namespace std;
SingleChannel::SingleChannel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleChannel),
    RubBand(new QRubberBand(QRubberBand::Rectangle, this)),
    Xup(0),
    Xdown(500),
    Yup(100),
    Ydown(-100)
{
    ui->setupUi(this);
    Init();
    m_colorlist<<QColor(255,0,0)<<QColor(0,255,0)<<QColor(0,0,255)<<QColor(0,0,0);
}

SingleChannel::~SingleChannel()
{
    RubBand->deleteLater();
    delete ui;
}
//右键菜单
void SingleChannel::contextMenuRequest(QPoint pos)
{
    qDebug()<<"右键菜单";
    QCustomPlot* customPlot=ui->Plot;
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("调整范围",this, SLOT(rescaleAxes()));
    menu->popup(customPlot->mapToGlobal(pos));
}

//调整到合适范围
void SingleChannel::rescaleAxes()
{
    qDebug()<<"调整到合适范围";
    QCustomPlot* customPlot=ui->Plot;
    customPlot->graph(0)->rescaleAxes();
    customPlot->graph(1)->rescaleAxes(true);
    customPlot->replot();
}
void SingleChannel::Init()
{
    //右键菜单自定义
    ui->Plot->setContextMenuPolicy(Qt::CustomContextMenu);
    //信号连接槽函数
//    connect(ui->Plot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));
//    connect(ui->Plot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePressEvent(QMouseEvent*)));
//    connect(ui->Plot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMoveEvent(QMouseEvent*)));
//    connect(ui->Plot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseReleaseEvent(QMouseEvent*)));
    QCustomPlot* customPlot=ui->Plot;

    setupAdvancedAxesDemo(customPlot);
    //选择框模式：无
    customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
    //选框黑色虚线
    customPlot->selectionRect()->setPen(QPen(Qt::black,1,Qt::DashLine));
    customPlot->selectionRect()->setBrush(QBrush(QColor(0,0,100,50)));
    //修改多选按键，默认Ctrl
    //customPlot->setMultiSelectModifier(Qt::KeyboardModifier::ControlModifier);
    //滚动缩放、图表可选、多选
    customPlot->setInteractions(QCP::iRangeZoom | QCP::iSelectPlottables| QCP::iMultiSelect);//
    connect(customPlot,SIGNAL(selectionChangedByUser()),this,SLOT(slot_selectionChangedByUser()));
    customPlot->replot();





}
void SingleChannel::clearPlot(){
    QCustomPlot* customPlot=ui->Plot;
    customPlot->clearGraphs();
}
void SingleChannel::setupAdvancedAxesDemo(QCustomPlot *customPlot)
{
    customPlot->plotLayout()->clear();   // 首先清空默认的轴矩形，让我们从头开始
    for(int i=0;i<5;i++){
        QCPAxisRect *wideAxisRect = new QCPAxisRect(customPlot, true);   // 还记得setupDefaultAxes为true时的作用吗
        wideAxisRect->axis(QCPAxis::atLeft)->setTickLabels(true); //刻度值不显示
        wideAxisRect->axis(QCPAxis::atLeft)->ticker()->setTickCount(2); // 设置轴的刻度为一个固定的步进值
        wideAxisRect->axis(QCPAxis::atLeft)->setTickLabelSide(QCPAxis::LabelSide::lsInside);
        wideAxisRect->axis(QCPAxis::atBottom)->grid()->setVisible(false);
        customPlot->plotLayout()->addElement(i, 0, wideAxisRect);     // 在第一行添加轴矩形
        // 保持一个好的习惯，将它们放置在相应的层
        foreach (auto *axis, wideAxisRect->axes()) {
            axis->setLayer("axes");
            axis->grid()->setLayer("grid");
        }

    }

}
QString SingleChannel::GetWaveInfo(const ChWaveInfo& Info)
{
    return QString("Ch:%1\nFreq:%2\nTime:%3\nValue:%4").
            arg(Info.ChNumber).
            arg(Info.Frequency).
            arg(Info.SampTime).
            arg(Info.Value);
}


void SingleChannel::mousePressEvent(QMouseEvent *e)
{
    qDebug()<<"mousePressEvent:"<<e->pos();
    if(!ui->Plot->viewport().contains(e->pos())) return;
}
void SingleChannel::mouseMoveEvent(QMouseEvent *e){
    qDebug()<<"mouseMoveEvent:"<<e->pos();
}
void SingleChannel::mouseReleaseEvent(QMouseEvent *e){
    qDebug()<<"mouseReleaseEvent:"<<e->pos();
//    if(!ui->Plot->viewport().contains(e->pos())) return;
//    if(e->button() == Qt::LeftButton) {
//        const QRect zoomRect = RubBand->geometry();
//        int xp1, yp1, xp2, yp2;
//        zoomRect.getCoords(&xp1, &yp1, &xp2, &yp2);
//        int padding = ui->cBoxWave->width() +
//                ui->Plot->yAxis->padding() +
//                ui->Plot->yAxis->offset() +
//                ui->Plot->yAxis->tickLabelPadding();
//        double x1 = static_cast<int>(ui->Plot->xAxis->pixelToCoord(xp1 - padding) + static_cast<double>(0.5f));
//        double x2 = static_cast<int>(ui->Plot->xAxis->pixelToCoord(xp2 - padding) + static_cast<double>(0.5f));
//        if(x2 - x1 > 1) {
//            // 放大选中区域波形，改变x轴范围
//            ui->Plot->xAxis->setRange(x1, x2);
//        }
//        else {
//            qDebug()<<"到时线";
//            // 到时线
//            QVector<double> x(2), y(2);
//            QRect view = ui->Plot->viewport();
////            QCPAxisRect* size = ui->Plot->axisRect();
//            x[0] = x1;
//            x[1] = x1;
//            y[0] = -(view.height() / 2);
//            y[1] = view.height() / 2;
//            ui->Plot->graph(1)->setData(x, y);
//            WaveInfo.SampTime = static_cast<float>(x[0]);
//            WaveInfo.Value = ui->Plot->graph(0)->data()->at(static_cast<int>(x[0]))->mainValue();
//            ui->WaveInfo->setText(GetWaveInfo(WaveInfo));
//        }
//        ui->Plot->replot();
//        RubBand->hide();
//    }
}
/**
 * @brief SingleChannel::addPlot
 * @param type
 * @param index
 * @param primary
 * @param m_comtradeRecord
 * @param m_analogSamples
 * @param m_digitalSamples
 * @param m_timeBaseus
 */
void SingleChannel::addPlot(ChannelType type, int index, bool primary, ComtradeRecord * m_comtradeRecord,QList<QVector<double>> m_analogSamples,QList<QVector<double>> m_digitalSamples,QVector<double> m_timeBaseus)
{
    QVector<double> data;
    bool digital = type == DigitalChannel;
    QString label;
    if (digital){
        if (index >= m_comtradeRecord->digitalChannelsCount()) {
            qWarning() << "Out of range digital channel index: " << index;
            return;
        }
        data = m_digitalSamples.value(index);
        label = m_comtradeRecord->digitalChannel(index)->id();
    }else{
        if (index >= m_comtradeRecord->analogChannelsCount()) {
            qWarning() << "Out of range analog channel index: " << index;
            return;
        }

        data = m_analogSamples.value(index);
        label = QStringLiteral("%1 (%2)").arg(m_comtradeRecord->analogChannel(index)->id())
                    .arg(m_comtradeRecord->analogChannel(index)->unit());
        qDebug()<<label;
    }
    QCPAxisRect* axisrect =ui->Plot->axisRect(index);
    QCPGraph *graph = ui->Plot->addGraph(axisrect->axis(QCPAxis::atBottom),axisrect->axis(QCPAxis::atLeft));
    if (!graph){
        qWarning() << "Can't create graph";
        return;
    }
    QColor m_color=m_colorlist.at(index%4);
    graph->setPen(QPen(m_color));
    graph->valueAxis()->setLabelColor(m_color);
    graph->valueAxis()->setLabel(label);
    graph->valueAxis()->setVisible(true);


    if(digital){
        graph->setLineStyle(QCPGraph::lsStepLeft);
    }else{
        graph->setLineStyle(QCPGraph::lsLine);
    }

    graph->setData(m_timeBaseus, data);
    //第二种表示最大值：
    auto max = std::max_element(std::begin(data), std::end(data));
    //最小值表示：
    auto min = std::min_element(std::begin(data), std::end(data));
    qDebug()<<"max:"<<*max<<"min:"<<*min;
//    graph->valueAxis()->setRangeUpper(*max);
//    graph->valueAxis()->setRangeLower(*min);
    double add=(*max-*min)/4;
    graph->valueAxis()->setRange(*min-add,*max+add);
    graph->rescaleAxes(true);
//    graph->valueAxis()->rescale(true);
    //数据多选
    graph->setSelectable(QCP::SelectionType::stMultipleDataRanges);
    // create connection between axes and scroll bars:
    connect(ui->horizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(horzScrollBarChanged(int)));
    connect(graph->keyAxis(), SIGNAL(rangeChanged(QCPRange)), this, SLOT(xAxisChanged(QCPRange)));

    // initialize axis range (and scroll bar positions via signals we just connected):
//    graph->keyAxis()->setRange(0, 1000, Qt::AlignCenter);
//     graph->valueAxis()->setRange(*min,*max);
//    graph->valueAxis()->setRange(-200, 400, Qt::AlignCenter);
//    ui->Plot->axisRect()->setupFullAxesBox(true);
//    ui->Plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);




    ui->Plot->replot();
}

void SingleChannel::setRangeLower(int index,int sample,QVector<double> m_timeBaseus)
{
    if (sample < 0 || sample >= m_timeBaseus.count()) {
        return;
    }
    ui->Plot->graph(index)->keyAxis()->setRangeLower((m_timeBaseus.value(sample)));
//    ui->Plot->xAxis->setRangeLower(m_timeBaseus.value(sample));
    // configure scroll bars:
    // Since scroll bars only support integer values, we'll set a high default range of -500..500 and
    // divide scroll bar position values by 100 to provide a scroll range -5..5 in floating point
    // axis coordinates. if you want to dynamically grow the range accessible with the scroll bar,
    // just increase the minimum/maximum values of the scroll bars as needed.
    ui->horizontalScrollBar->setRange(m_timeBaseus.value(sample), m_timeBaseus.value(m_timeBaseus.count()-1));

}

void SingleChannel::setRangeUpper(int index,int sample,QVector<double> m_timeBaseus)
{
    if (sample < 0) {
        return;
    } else if (sample >= m_timeBaseus.count()) {
        sample = m_timeBaseus.count() - 1;
    }
    ui->Plot->graph(index)->keyAxis()->setRangeUpper(m_timeBaseus.value(sample));
}

bool SingleChannel::savePng(QCustomPlot *plot,const QString &fileName, int width, int height)
{
    return plot->savePng(fileName, width, height);
}
//选择的数据变化,根据选择区域获取数据元素
void SingleChannel::slot_selectionChangedByUser()
{
   QCustomPlot* customPlot=ui->Plot;
//    //清空listwidget
   m_selectdata.clear();
   qDebug()<<"customPlot.graphCount:"<<customPlot->graphCount();
   QCPDataSelection selection;
    for(int i=0;i<customPlot->graphCount();i++)
    {
        //遍历有被选中的graph
        if(customPlot->graph(i)->selected())
        {
            selection =customPlot->graph(i)->selection();

            break;
        }
    }
    for(int i=0;i<customPlot->graphCount();i++)
    {
        m_selectdata.clear();
        pointlist[i].clear();
        //遍历选中范围
        for(int j=0;j<selection.dataRangeCount();j++)
        {
            QCPDataRange dataRange = selection.dataRange(j);
            int count=0;
            //遍历数据
            for(int k=dataRange.begin();k<dataRange.end();k++)
            {
                QString str_key = QString::number(customPlot->graph(i)->data()->at(k)->key);
                QString str_value = QString::number(customPlot->graph(i)->data()->at(k)->value);
                QString str_at= QString::number(i);
                //添加到listwidget
                m_selectdata.append("("+str_key+", "+str_value+")");
                pointlist[i].append(QPointF(str_key.toDouble(),str_value.toDouble()));
                   count++;
            }
            qDebug()<<"count"<<count;
            if(count<100){
                QUIHelper::showMessageBoxError(QString("没有铁磁谐振！(%1points)").arg(count));
            }

        }
//        qDebug()<<QString("pointlist[%1]").arg(i)<<pointlist[i];
    }

}

void SingleChannel::on_btn_zoomIn_clicked(bool checked)
{

        QCustomPlot* customPlot=ui->Plot;
        if(!checked)
        {
            ui->btn_move->setChecked(false);//取消拖动选项
            customPlot->setInteraction(QCP::iRangeDrag,false);//取消拖动
            ui->btn_select->setChecked(false);//取消选择
            customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmZoom);
        }
        else
        {
            customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
        }
        ui->btn_select->setChecked(!checked);
}



void SingleChannel::on_btn_select_clicked(bool checked)
{
    QCustomPlot* customPlot=ui->Plot;
    if(!checked)
    {
        ui->btn_zoomIn->setChecked(false);//取消放大
        ui->btn_select->setChecked(false);//取消拖动选项
        customPlot->setInteraction(QCP::iRangeDrag,false);//取消拖动
        customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmSelect);

    }
    else
    {
        customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
    }
    ui->btn_select->setChecked(!checked);
}


void SingleChannel::on_btn_move_clicked(bool checked)
{
    qDebug()<<"checked:"<<checked;
    QCustomPlot* customPlot=ui->Plot;
    if(!checked)
    {
        ui->btn_zoomIn->setChecked(false);//取消放大
        ui->btn_select->setChecked(false);//取消选择
        customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
        customPlot->setInteraction(QCP::iRangeDrag,true);//使能拖动
    }
    else
    {
        customPlot->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
        customPlot->setInteraction(QCP::iRangeDrag,false);//取消拖动
    }
    ui->btn_select->setChecked(!checked);
}

void SingleChannel::horzScrollBarChanged(int value)
{
//  qDebug()<<"value"<<value;
  if (qAbs(ui->Plot->xAxis->range().center()-value/100.0) > 0.01) // if user is dragging plot, we don't want to replot twice
  {
    for(int i=0;i<5;i++){
        ui->Plot->graph(i)->keyAxis()->setRange(value, ui->Plot->xAxis->range().size(), Qt::AlignCenter);
        ui->Plot->replot();
    }
  }
}


void SingleChannel::xAxisChanged(QCPRange range)
{
//  qDebug()<<"range"<<range <<range.center()<<range.size();
//  ui->horizontalScrollBar->setValue(qRound(range.center()*100.0)); // adjust position of scroll bar slider
//  ui->horizontalScrollBar->setPageStep(qRound(range.size())); // adjust size of scroll bar slider
}


