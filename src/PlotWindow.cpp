﻿#include "PlotWindow.h"
#include "ui_plotwindow.h"

PlotWindow::PlotWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::PlotWindow)
{
    ui->setupUi(this);

}

PlotWindow::~PlotWindow()
{
    delete ui;
}

void PlotWindow::initComtrade(QString cfgPath,QString datPath){

    QFile cfgFile(cfgPath);
    if (!cfgFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Can't open cfg file: " << cfgPath;

    }
    ComtradeRecord *record = ComtradeRecord::fromCfg(&cfgFile);
    QFile datFile(datPath);
    if (!datFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Can't open dat file: " << datPath;
    }
    setComtradeRecord(record);
    loadSamples(&datFile);
    m_plotlist.clear();
    ui->channel1->clearPlot();
    //界面图表控件加入容器
    m_plotlist.append(ui->channel1);

    for(int i=0;i<5;i++){//record->channelsCount()
        if (i<record->analogChannelsCount()) {
            m_plotlist.at(0)->addPlot(SingleChannel::AnalogChannel,i,true,m_comtradeRecord,m_analogSamples,m_digitalSamples,m_timeBaseus);//绘制到
            m_plotlist.at(0)->setRangeLower(i,0,m_timeBaseus);
            m_plotlist.at(0)->setRangeUpper(i,800,m_timeBaseus);
        }else{
            m_plotlist.at(1)->addPlot(SingleChannel::DigitalChannel,i,true,m_comtradeRecord,m_analogSamples,m_digitalSamples,m_timeBaseus);
            m_plotlist.at(1)->setRangeLower(i,0,m_timeBaseus);
            m_plotlist.at(1)->setRangeUpper(i,800,m_timeBaseus);

        }

    }
    qDebug()<<"m_plotlist"<<m_plotlist.length();

//        QString outPath = parser.value(outOpt);
//        QStringList dimensionsTokens = parser.value(dimensionsOpt).toLower().split(QStringLiteral("x"));
//        int width = dimensionsTokens.value(0).toInt();
//        int height = dimensionsTokens.value(1).toInt();
//        w.savePng(outPath, width, height);
}
void PlotWindow::setComtradeRecord(ComtradeRecord *comtradeRecord)
{
    m_comtradeRecord = comtradeRecord;
}

bool PlotWindow::loadSamples(QIODevice *samplesFile)
{
    if (!m_comtradeRecord) {
        qWarning() << "m_comtradeRecord is null";
        return false;
    }

    m_analogSamples.clear();
    m_digitalSamples.clear();
    m_timeBaseus.clear();

    int lastRate = m_comtradeRecord->ratesCount() - 1;
    int totalSamples = m_comtradeRecord->lastSample(lastRate);
    int channelsCount = m_comtradeRecord->channelsCount();
    int analogChannelsCount = m_comtradeRecord->analogChannelsCount();
    int digitalChannelsCount = m_comtradeRecord->digitalChannelsCount();

    // Pre-populate empty sample vectors
    for (int i = 0; i < analogChannelsCount; ++i) {
        QVector<double> samples;
        samples.resize(totalSamples);
        m_analogSamples.append(samples);
    }
    for (int i = 0; i < digitalChannelsCount; ++i) {
        QVector<double> samples;
        samples.resize(totalSamples);
        m_digitalSamples.append(samples);
    }

    m_timeBaseus.resize(totalSamples);
    bool hasFixedRate = m_comtradeRecord->ratesCount() > 0;

    // If we have fixed rates, we use this as timebase
    if (hasFixedRate) {
        int currentRate = 0;
        double currentSampling = m_comtradeRecord->sampleRate(currentRate);
        int currentLastSample = m_comtradeRecord->lastSample(currentRate);
        for (int i = 0; i < totalSamples; ++i) {
            if (i > currentLastSample) {
                currentRate++;
                currentSampling = m_comtradeRecord->sampleRate(currentRate);
                currentLastSample = m_comtradeRecord->lastSample(currentRate);
            }
            m_timeBaseus[i] = (1 / currentSampling) * i * 1000 - m_comtradeRecord->triggerOffsetms();
        }
    }

    QByteArray line;
    int lineCount = 0;
    QString datType=m_comtradeRecord->datType();
    qDebug()<<"datType"<<datType;
/************************************************************************************************************
* 对于Binary保存方式的数据文件（dat文件），数据保存格式为：序号[4字节]、采样时间[4字节]、模拟量值[按cfg文件的顺序，两字节
* 表示一个模拟量采样值]、数字量组值[按cfg文件的顺序，16个数字量一组，一组用2字节表示，不够一组的空位（高位）补零构成一组，组内
* 从低位bit依次到高位bit与cfg文件里的数字量顺序对应。
*************************************************************************************************************/
    if(datType.contains("BINARY")){//二进制格式解析,只支持数字通道
         qDebug()<<"datType"<<datType;
         while(lineCount < totalSamples) {
             QByteArray index = samplesFile->read(4);
             QByteArray timestamp = samplesFile->read(4);
//             qDebug()<<QUIHelper::byteToIntRec(index)<<QUIHelper::byteToIntRec(timestamp);

             for(int i=0;i<analogChannelsCount;i++){
                 QByteArray data_a=samplesFile->read(2);
                 const ComtradeAnalogChannel *analogChannel = m_comtradeRecord->analogChannel(i);
                 double sample = analogChannel->multiplier() * QUIHelper::byteToUShortRec(data_a) + analogChannel->offset();
//                 qDebug()<<"sample:"<<sample;
                 m_analogSamples[i][lineCount] = sample;
             }
             if(digitalChannelsCount>0){
                 QByteArray data_d=samplesFile->read(2);
                 int data_d_t=QUIHelper::byteToIntRec(data_d);
                 for (int i = 0; i < digitalChannelsCount; ++i) {
                     double sample =((data_d_t>>i)&0x01);
                     m_digitalSamples[i][lineCount] = sample;
                 }
             }


             lineCount++;
//             qDebug()<<"line:"<<QUIHelper::byteArrayToHexStr(line);
         }
    }else{//ASNI格式解析方式
        for (line = samplesFile->readLine(); !line.isEmpty() && lineCount < totalSamples; line = samplesFile->readLine()) {
            QList<QByteArray> tokens = line.trimmed().split(',');
            // n, timestamp, 1 sample per channel
            if (tokens.length() != channelsCount + 2) {
                qWarning() << "Invalid data line: " << line << ", skipping it";
                continue;
            }
            if (!hasFixedRate) {
                int timestamp = tokens.value(1).toInt() * m_comtradeRecord->timeMultiplier();
                m_timeBaseus[lineCount] = timestamp;
            }
            for (int i = 0; i < analogChannelsCount; ++i) {
                const ComtradeAnalogChannel *analogChannel = m_comtradeRecord->analogChannel(i);
                double sample = analogChannel->multiplier() * tokens.value(i + 2).toDouble() + analogChannel->offset();
                m_analogSamples[i][lineCount] = sample;
            }
            for (int i = 0; i < digitalChannelsCount; ++i) {
                double sample = tokens.value(i + analogChannelsCount + 2).toDouble();
                m_digitalSamples[i][lineCount] = sample;
            }
            lineCount++;
        }
    }
    if (lineCount != totalSamples) {
        qWarning() << "DAT files has" << lineCount << "lines, but" << totalSamples << "samples were expected";
        return false;
    }
    return true;
}



void PlotWindow::on_channel2_windowIconChanged(const QIcon &icon)
{

}

/**
 * @brief PlotWindow::on_btn_openFile_released
 * 选择cfg文件，自动查找对饮的dat文件
 */
void PlotWindow::on_btn_openFile_released()
{
    QString cfgFile=QUIHelper::getFileName("cfg(*.cfg)","./");
    ui->lEdit_cfg->setText(cfgFile);
    QString datFile=QString("%1.dat").arg(cfgFile.left(cfgFile.lastIndexOf(".")));

    if(QFile::exists(cfgFile)&&QFile::exists(datFile)){
        initComtrade(cfgFile,datFile);
    }else{
        QUIHelper::showMessageBoxError("选择文件失败，请检查是否存在同名cgf、dat文件！");
    }

}

