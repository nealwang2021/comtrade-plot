﻿#include <QApplication>
#include "PlotWindow.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    PlotWindow w;
    w.show();
    return app.exec();
}
